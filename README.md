# Build Scripts

This repository contains build and automation scripts I use for my research.

## InstallCrossCompiler.sh

The `InstallCrossCompiler.sh` script installs a MicroBlaze and an ARM cross compiler for bare metal application.
See the `docs` directory for a detailed description of this script.


## MakeBuildScripsArchive.sh

**The `MakeBuildScriptsArchive.sh` is deprecated and got replaced by new scripts.**

The new scripts will be uploaded as soon as they are easy to handle (for other than the developer).

The `MakeBuildScriptsArchive.sh` creates an archive (.tar.gz) with a set of build scripts.
This archive must be extracted inside the workspace directory it shall be used.
A documentation of the individual scripts follows in the (far) future.


