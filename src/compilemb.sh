#!/usr/bin/env bash
#
# Version: 1.0.0
#
# Changelog:
#  1.0.0 - 11.03.19: First release
#
# Contributors:
#  Ralf Stemmer - ralf.stemmer@uni-oldenburg.de
#

source ./scripts/settings.sh
LIBPATHS="-L$SDKDIR/lib"
LIBS="-lxil -lgloss -lgcc -lc"
INCLUDEPATHS="-I$SDKDIR/include"
CFLAGS="-O0 -c -mcpu=v9.6 -mlittle-endian -mxl-soft-mul"
LFLAGS="-Wl,--no-relax -mcpu=v9.6 -mlittle-endian -mxl-soft-mul"
GCC=mb-gcc
OBJCOPY=mb-objcopy

if [ -f "$SW_SRCDIR/setup.sh" ] ; then
    echo -e -n "\e[1;34mLoading software specific setup: "
    source $SW_SRCDIR/setup.sh
    echo -e "\e[1;32m✔\e[0m"
fi

if [ -d "$SW_BUILDDIR" ] ; then
    echo -e -n "\e[1;34mRemoving old object-files: "
    find "$SW_BUILDDIR" -type f -name "*.o" -delete
    echo -e "\e[1;32m✔\e[0m"
fi

mkdir -p $SW_BUILDDIR

# Compile shared code
echo -e "\e[1;36mCompiling shared code \e[0;36m…\e[0m"
mkdir -p $SW_BUILDDIR/share

SOURCES=$(find $SW_SRCDIR/share -type f -name "*.c")
for s in $SOURCES ; 
do
    echo -e -n "\e[1;34m - $s\e[0m "
    FILENAME="${s##*/}"
    OBJFILE="${FILENAME%.*}.o"
    $GCC $CFLAGS $LIBPATHS $INCLUDEPATHS -I$SW_SRCDIR/share -c -o "$SW_BUILDDIR/share/$OBJFILE" $s
    if [ $? -ne 0 ] ; then
        echo -e "\e[1;31m$GCC FAILED!\e[0m"
        exit 1
    else
        echo -e "\e[1;32m✔\e[0m"
    fi
done

# First argument must be the number of the processor ("0", "1", …)
# Second argument can be an address with a dot as prefix (".4000000", ".C0000000")
function CompileForProcessor
{
    MBNUM=${1}
    ADDRESS=${2:-""}
    SDKINCLUDE="$SDKDIR/include/mb$MBNUM"
    if [ -z $ADDRESS ] ; then
        SOURCEDIR="$SW_SRCDIR/mb${MBNUM}"
        BUILDDIR="$SW_BUILDDIR/mb${MBNUM}"
    else
        SOURCEDIR="$SW_SRCDIR/mb${MBNUM}.${ADDRESS}" # leads to …/mb0.40000000
        BUILDDIR="$SW_BUILDDIR/mb${MBNUM}.${ADDRESS}"
    fi

    echo -e "\e[1;36mCompiling for MicroBlaze $MBNUM \e[1;35m$ADDRESS\e[0m"

    # Check environment
    echo -e "\e[1;34mChecking source environment"
    echo -e -n "\t\e[1;34m$SDKINCLUDE: "
    if [ ! -d "$SDKINCLUDE" ] ; then
        echo -e "\e[1;31m$SDKINCLUDE missing! Rebuild SDK or check MicroBlaze number!"
        exit 1
    else
        echo -e "\e[1;32m✔\e[0m"
    fi

    echo -e -n "\t\e[1;34m$SOURCEDIR: "
    if [ ! -d "$SOURCEDIR" ] ; then
        echo -e "\e[1;31m$SOURCEDIR missing! Check MicroBlaze number and/or address!"
        exit 1
    else
        echo -e "\e[1;32m✔\e[0m"
    fi

    mkdir -p $BUILDDIR

    # Compile
    SOURCES=$(find $SOURCEDIR -type f -name "*.c")
    for s in $SOURCES ;
    do
        echo -e -n "\e[1;34m - $s\e[0m "
        FILENAME="${s##*/}"
        OBJFILE="${FILENAME%.*}.o"
        $GCC $CFLAGS $LIBPATHS $INCLUDEPATHS -I$SDKINCLUDE -I$SOURCEDIR -I$SW_SRCDIR/share -c -o "$BUILDDIR/$OBJFILE" $s
        if [ $? -ne 0 ] ; then
            echo -e "\e[1;31m$GCC FAILED!\e[0m"
            exit 1
        else
            echo -e "\e[1;32m✔\e[0m"
        fi
    done
}

# Linken
# First argument must be the number of the processor ("0", "1", …)
# Second argument can be an address with a dot as prefix (".4000000", ".C0000000")
function LinkFirmwareForProcessor
{
    MBNUM=${1}
    ADDRESS=${2:-""}
    #SDKINCLUDE="$SDKDIR/include/mb$MBNUM"
    if [ -z $ADDRESS ] ; then
        SOURCEDIR="$SW_SRCDIR/mb${MBNUM}"
        BUILDDIR="$SW_BUILDDIR/mb${MBNUM}"
        FIRMWARE="$OUTPUTDIR/firmware_mb${MBNUM}.elf"
        # prefere explicit linker script. Use SDK generated as fallback
        if [ -f $SOURCEDIR/ldscript.ld ] ; then
            LINKSCRIPT="$SOURCEDIR/ldscript.ld"
        else
            LINKSCRIPT="$SDKDIR/ldscript_mb$MBNUM.ld"
        fi

    else
        SOURCEDIR="$SW_SRCDIR/mb${MBNUM}.${ADDRESS}" # leads …/mb0.40000000
        BUILDDIR="$SW_BUILDDIR/mb${MBNUM}.${ADDRESS}"
        FIRMWARE="$OUTPUTDIR/firmware_mb${MBNUM}.${ADDRESS}.elf"
        LINKSCRIPT="$SOURCEDIR/ldscript.ld"
    fi

    PROCSOURCES=$(find $BUILDDIR          -type f -name "*.o")
    SHARSOURCES=$(find $SW_BUILDDIR/share -type f -name "*.o")

    echo -e "\e[1;36mLinking \e[0;36m$FIRMWARE\e[1;34m: "

    # Check environment
    echo -e "\e[1;34mChecking source environment"
    echo -e -n "\t\e[1;34mLinker script $LINKSCRIPT: "
    if [ ! -f "$LINKSCRIPT" ] ; then
        echo -e "\e[1;31m$LINKSCRIPT missing! Rebuild SDK or check MicroBlaze number!"
        exit 1
    else
        echo -e "\e[1;32m✔\e[0m"
    fi

    echo -e -n "\t\e[1;34mHAL $SDKDIR/lib/mb$MBNUM: "
    if [ ! -d "$SDKDIR/lib/mb$MBNUM" ] ; then
        echo -e "\e[1;31m$SDKDIR/lib/mb$MBNUM missing! Rebuild SDK or check MicroBlaze number!"
        exit 1
    else
        echo -e "\e[1;32m✔\e[0m"
    fi

    echo -e -n "\e[1;34mLinking "
    $GCC -Wl,-T -Wl,$LINKSCRIPT $LFLAGS $LIBPATHS -L$SDKDIR/lib/mb$MBNUM -o $FIRMWARE $PROCSOURCES $SHARSOURCES $LIBS
    if [ $? -ne 0 ] ; then
        echo -e "\e[1;31mFAILED!\e[0m"
        exit 1
    else
        echo -e "\e[1;32m✔\e[0m"
    fi

    $OBJCOPY -O binary $FIRMWARE ${FIRMWARE%.*}.bin
    chmod -x $FIRMWARE
    chmod -x ${FIRMWARE%.*}.bin
}

# Count microblazes
MICROBLAZECOUNT=$( grep InstPath $SDK_MEMORYINFO | grep -i microblaze | wc -l )

# Build main firmware for each MicroBlaze
for MBNR in $(seq 0 $((MICROBLAZECOUNT - 1)) ) ; do 
    CompileForProcessor $MBNR
    LinkFirmwareForProcessor $MBNR
done

# Now scan source directory for additional firmware mapped to different memory
MAPPINGS=$(find $SW_SRCDIR -type d -name "mb*.*" -printf "%f\n")
for m in $MAPPINGS ; do 
    MB=${m%.*}
    MB=${MB:2}
    ADDR=${m:4}
    CompileForProcessor $MB $ADDR
    LinkFirmwareForProcessor $MB $ADDR
done


echo -e "\e[1;32mNew System built.\e[0m"

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

