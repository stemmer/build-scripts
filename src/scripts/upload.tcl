
#
# Version: 1.0.0
#
# Changelog:
#  1.0.0 - 11.03.19: First release
#
# Contributors:
#  Ralf Stemmer - ralf.stemmer@uni-oldenburg.de
#

puts "\x1B\[1;36mUploading bitstream using Vivado Design Suite [version -short]\x1B\[0m"

# Define environment
set BITSTREAM "[lindex $argv 0]"

puts "\x1B\[1;34mUploading \x1B\[0;36m$BITSTREAM\x1B\[0m"

open_hw
connect_hw_server
open_hw_target

set_property PROGRAM.FILE $BITSTREAM [lindex [get_hw_devices xc7z020_1] 0]
current_hw_device [lindex [get_hw_devices xc7z020_1] 0]
refresh_hw_device -update_hw_probes false [lindex [get_hw_devices xc7z020_1] 0]

set_property PROBES.FILE {} [lindex [get_hw_devices xc7z020_1] 0]
set_property PROGRAM.FILE $BITSTREAM [lindex [get_hw_devices xc7z020_1] 0]
program_hw_devices [lindex [get_hw_devices xc7z020_1] 0]
refresh_hw_device [lindex [get_hw_devices xc7z020_1] 0]

puts "\x1B\[1;32mdone\x1B\[0m"

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

