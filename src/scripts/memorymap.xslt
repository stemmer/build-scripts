<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text"/>
    <xsl:template match="/">

        <xsl:for-each select="EDKSYSTEM/MODULES/MODULE/MEMORYMAP/MEMRANGE">
            <xsl:value-of select="@INSTANCE"  /> <xsl:text>,</xsl:text>
            <xsl:value-of select="@BASEVALUE" /> <xsl:text>,</xsl:text>
            <xsl:value-of select="@HIGHVALUE" /> <xsl:text>&#x0A;</xsl:text>
        </xsl:for-each>

    </xsl:template>
</xsl:stylesheet>
