
#
# Version: 1.0.0
#
# Changelog:
#  1.0.0 - 11.03.19: First release
#
# Contributors:
#  Ralf Stemmer - ralf.stemmer@uni-oldenburg.de
#

puts "\x1B\[1;36mCreating SDK for ARM CortexA9 [lindex $argv 1] using xsdk\x1B\[0m"

# Define environment
source ./scripts/settings.tcl
set SDKDIR "$OUTPUTDIR/$PROJECTNAME.sdk"
set PROCESSOR "ps7_cortexa9_[lindex $argv 1]"
set HWNAME  "hardware_arm[lindex $argv 1]"
set SWNAME  "software_arm[lindex $argv 1]"
set BSPNAME "board_arm[lindex $argv 1]"


# Create output directory
file mkdir $SDKDIR
#set_workspace $SDKDIR
setws $SDKDIR

puts "\x1B\[1;34mCreating BSP…\x1B\[0m"

createhw  -name $HWNAME  -hwspec $HARDWAREDEF
createbsp -name $BSPNAME -hwproject $HWNAME -proc $PROCESSOR -os standalone

puts "\x1B\[1;34mAdding libraries to BSP…\x1B\[0m"
openbsp $BSPNAME
setlib -bsp $BSPNAME -lib xilffs
regenbsp -bsp $BSPNAME
closebsp $BSPNAME

puts "\x1B\[1;34mCreating FSBL…\x1B\[0m"
createapp          -name $SWNAME  -hwproject $HWNAME -proc $PROCESSOR -os standalone -lang C -app {Zynq FSBL}   -bsp $BSPNAME

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

