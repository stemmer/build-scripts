
#
# Version: 1.0.0
#
# Changelog:
#  1.0.0 - 11.03.19: First release
#
# Contributors:
#  Ralf Stemmer - ralf.stemmer@uni-oldenburg.de
#

puts "\x1B\[1;36mUploading system using XSCT\x1B\[0m"

source ./scripts/settings.tcl
source $PROJECTDIR/sdk/ps7_init.tcl

connect


# ARM 0
puts "\x1B\[1;34mUploading software \x1B\[0;36mfirmware_arm0\x1B\[0m"
targets -set -filter {name =~ "ARM*#0"}
rst

loadhw $HARDWAREDEF
ps7_init
ps7_post_config

dow -clear $OUTPUTDIR/firmware_arm0.elf
if { [ catch { verify $OUTPUTDIR/firmware_arm0.elf } ERROR ] } {
    puts stderr "\x1B\[1;31mVerifying $OUTPUTDIR/firmware_arm0.elf upload failed!\n\t$ERROR"
    exit 1
}

#########################
#set BINARIES [ glob -directory $OUTPUTDIR -type f *.bin ]
#foreach BINARYPATH $BINARIES {
#    set BINARYNAME [file tail $BINARYPATH]
#    if { [ string match "firmware_mb0.*.bin" $BINARYNAME ] } {
#        regexp {\.(.*)\.} "$BINARYNAME" -> ADDRESS
#        puts "\t\x1B\[1;34mUploading \x1B\[0;36m$BINARYNAME\x1B\[1;34m to address \x1B\[1;35m0x$ADDRESS\x1B\[0m"
#
#        dow -data $OUTPUTDIR/$BINARYNAME "0x00100000"
#    }
#}
#set BINARIES [ glob -directory $OUTPUTDIR -type f *.bin ]
#foreach BINARYPATH $BINARIES {
#    set BINARYNAME [file tail $BINARYPATH]
#    if { [ string match "firmware_mb1.*.bin" $BINARYNAME ] } {
#        regexp {\.(.*)\.} "$BINARYNAME" -> ADDRESS
#        puts "\t\x1B\[1;34mUploading \x1B\[0;36m$BINARYNAME\x1B\[1;34m to address \x1B\[1;35m0x$ADDRESS\x1B\[0m"
#
#        dow -data $OUTPUTDIR/$BINARYNAME "0x00200000"
#    }
#}
#########################

con

# ARM 1
#targets -set -filter {name =~ "ARM*#1"}


# FPGA
puts "\x1B\[1;34mUploading hardware \x1B\[0;36m$BITSTREAM\x1B\[0m"
targets -set -filter {name =~ "xc7z020"}
fpga $BITSTREAM


# Upload the MicroBlaze software

# MicroBlaze 0
puts "\x1B\[1;34mUploading software \x1B\[0;36mfirmware_mb0\x1B\[0m"
targets -set -filter {name =~ "MicroBlaze*#0"}
rst

# Find all binaries and filter those with explicit addresses and upload them
set BINARIES [ glob -directory $OUTPUTDIR -type f *.elf ]
foreach BINARYPATH $BINARIES {
    set BINARYNAME [file tail $BINARYPATH]
    if { [ string match "firmware_mb0.*.elf" $BINARYNAME ] } {
        regexp {\.(.*)\.} "$BINARYNAME" -> ADDRESS
        puts "\t\x1B\[1;34mUploading \x1B\[0;36m$BINARYNAME\x1B\[1;34m to address \x1B\[1;35m0x$ADDRESS\x1B\[0m"

        dow -clear $OUTPUTDIR/$BINARYNAME

        if { [ catch { verify $OUTPUTDIR/$BINARYNAME } ERROR ] } {
            puts stderr "\x1B\[1;31mVerifying $OUTPUTDIR/$BINARYNAME upload failed!\n\t$ERROR"
            exit 1
        }
    }
}

puts "\t\x1B\[1;34mUploading FSBL \x1B\[0;36m$OUTPUTDIR/firmware_mb0.elf\x1B\[0m"
dow -clear $OUTPUTDIR/firmware_mb0.elf
if { [ catch { verify $OUTPUTDIR/firmware_mb0.elf } ERROR ] } {
    puts stderr "\x1B\[1;31mVerifying $OUTPUTDIR/firmware_mb0.elf upload failed!\n\t$ERROR"
    exit 1
}

con

# MicroBlaze 1
puts "\x1B\[1;34mUploading software \x1B\[0;36mfirmware_mb1\x1B\[0m"
targets -set -filter {name =~ "MicroBlaze*#1"}

# Find all binaries and filter those with explicit addresses and upload them
set BINARIES [ glob -directory $OUTPUTDIR -type f *.elf ]
foreach BINARYPATH $BINARIES {
    set BINARYNAME [file tail $BINARYPATH]
    if { [ string match "firmware_mb1.*.elf" $BINARYNAME ] } {
        regexp {\.(.*)\.} "$BINARYNAME" -> ADDRESS
        puts "\t\x1B\[1;34mUploading \x1B\[0;36m$BINARYNAME\x1B\[1;34m to address \x1B\[1;35m0x$ADDRESS\x1B\[0m"

        dow -clear $OUTPUTDIR/$BINARYNAME

        if { [ catch { verify $OUTPUTDIR/$BINARYNAME } ERROR ] } {
            puts stderr "\x1B\[1;31mVerifying $OUTPUTDIR/$BINARYNAME upload failed!\n\t$ERROR"
            exit 1
        }
    }
}

puts "\t\x1B\[1;34mUploading FSBL \x1B\[0;36m$OUTPUTDIR/firmware_mb1.elf\x1B\[0m"
dow -clear $OUTPUTDIR/firmware_mb1.elf
if { [ catch { verify $OUTPUTDIR/firmware_mb1.elf } ERROR ] } {
    puts stderr "\x1B\[1;31mVerifying $OUTPUTDIR/firmware_mb1.elf upload failed!\n\t$ERROR"
    exit 1
}

con



disconnect

puts "\x1B\[1;32mdone\x1B\[0m"

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

