
#
# Version: 1.0.0
#
# Changelog:
#  1.0.0 - 11.03.19: First release
#
# Contributors:
#  Ralf Stemmer - ralf.stemmer@uni-oldenburg.de
#

puts "\x1B\[1;36mCreating SDK for Microblaze [lindex $argv 1] using xsdk\x1B\[0m"

# Define environment
source ./scripts/settings.tcl
set SDKDIR "$OUTPUTDIR/$PROJECTNAME.sdk"
set PROCESSOR "microblaze_[lindex $argv 1]"
set HWNAME  "hardware_mb[lindex $argv 1]"
set SWNAME  "software_mb[lindex $argv 1]"
set BSPNAME "board_mb[lindex $argv 1]"


# Create output directory
file mkdir $SDKDIR
set_workspace $SDKDIR

puts "\x1B\[1;34mCreating BSP…\x1B\[0m"

create_hw_project  -name $HWNAME  -hwspec $HARDWAREDEF
create_bsp_project -name $BSPNAME -hwproject $HWNAME -proc $PROCESSOR -os standalone
create_app_project -name $SWNAME  -hwproject $HWNAME -proc $PROCESSOR -os standalone -lang C -app {Hello World} -bsp $BSPNAME

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

