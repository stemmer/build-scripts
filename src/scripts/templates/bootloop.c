// Xilinx Libraries
#include <xparameters.h>
#include <xil_io.h>
#include <xil_cache.h>

int main()
{
    Xil_ICacheEnable();
    Xil_DCacheEnable();

    while(1);

    return 0;   // Satisfy compiler
}

// vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

