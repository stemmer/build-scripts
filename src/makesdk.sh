#!/usr/bin/env bash
#
# Version: 1.0.0
#
# Changelog:
#  1.0.0 - 11.03.19: First release
#
# Contributors:
#  Ralf Stemmer - ralf.stemmer@uni-oldenburg.de
#

source ./scripts/settings.sh

# Check environment 
if [ -d "$SDKDIR" ] ; then
    echo -e "\e[1;31mSDK-Directory $SDKDIR does already exist."
    echo -e "\e[1;30mTo create a new SDK, the old one must be removed manually."
    exit 1
fi
if [ -d "$XSDKSDKDIR" ] ; then
    echo -e "\e[1;31mXSDK-SDK-Directory $XSDKSDKDIR does already exist."
    echo -e "\e[1;30mTo create a new SDK, the old one must be removed manually."
    exit 1
fi

if ! type "unzip" 2> /dev/null > /dev/null ; then
    echo -e "\e[1;31mThe tool unzip is missing."
    echo -e "\e[1;30mTo run this script, you need to install unzip."
    exit 1
fi

# Prepare environment
echo -e -n "\e[1;34mCreating SDK tree in \e[0;36m$SDKDIR\e[1;34m: "
mkdir -p $SDKDIR/lib
if [ $? -ne 0 ] ; then
    echo -e "\e[1;31mFAILED!\e[0m"
    exit 1
else
    echo -e "\e[1;32m✔\e[0m"
fi

# Extract System Definitions from Vivado Project
echo -e -n "\e[1;34mFinding System Definition "
SYSDEF=$(find $OUTPUTDIR/$PROJECTNAME.runs -name "*.sysdef" -print)
cp $SYSDEF $SDKDIR/sysdef.zip
if [ $? -ne 0 ] ; then
    echo -e "\e[1;31mFAILED!\e[0m"
    exit 1
else
    echo -e "\e[1;32m✔\e[0m"
fi

echo -e "\e[1;34mExtracting System Definition \e[0;36m$SYSDEF\e[0m"
unzip $SDKDIR/sysdef.zip -d $SDKDIR/.
if [ $? -ne 0 ] ; then
    echo -e "\e[1;31munzip FAILED!\e[0m"
    exit 1
fi

# Create Memorymap
echo -e -n "\e[1;34mCreating MemoyMap \e[0;36m$SDK_MEMORYMAP\e[1;34m: "
xsltproc ./scripts/memorymap.xslt $SDKDIR/$HARDWARENAME.hwh > $SDK_MEMORYMAP
if [ $? -ne 0 ] ; then
    echo -e "\e[1;31mFAILED!\e[0m"
    exit 1
else
    echo -e "\e[1;32m✔\e[0m"
fi

# Check if the Synthesis process was successful
echo -e -n "\e[1;34m$HARDWAREDEF: "
if [ ! -f "$HARDWAREDEF" ] ; then
    echo -e "\e[1;31m$HARDWAREDEF missing! Did you export the Hardware?"
    exit 1
else
    echo -e "\e[1;32m✔\e[0m"
fi

# Renaming Memory-Info file
MEMINFOR=$( find $SDKDIR     -name "*.mmi" -print)
echo -e -n "\e[1;34m$SDK_MEMORYINFO: "
mv $MEMINFOR        $SDK_MEMORYINFO
if [ $? -ne 0 ] ; then
    echo -e "\e[1;31mFAILED!\e[0m"
    exit 1
else
    echo -e "\e[1;32m✔\e[0m"
fi

# Make everything clean
BITSTREAM=$(find $SDKDIR     -name "*.bit" -print)
echo -e -n "\e[1;34m$SDK_BITFILE: "
mv $BITSTREAM       $SDK_BITFILE
if [ $? -ne 0 ] ; then
    echo -e "\e[1;31mFAILED!\e[0m"
    exit 1
else
    echo -e "\e[1;32m✔\e[0m"
fi

echo -e -n "\e[1;34m$SDK_HARDWAREDEF: "
cp $HARDWAREDEF     $SDK_HARDWAREDEF
if [ $? -ne 0 ] ; then
    echo -e "\e[1;31mFAILED!\e[0m"
    exit 1
else
    echo -e "\e[1;32m✔\e[0m"
fi


echo -e -n "\e[1;34mChecking Architecture "

# Get Architecture
#  Source: https://stackoverflow.com/questions/25508512/shellscript-read-xml-attribute-value
ARCHITECTURE=$(echo 'cat //Project/SYSTEMINFO/@ARCH' | xmllint --shell "$SDK_SYSTEMDEF" | grep ARCH | grep -v cat | cut -d "\"" -f 2)
if [ "$ARCHITECTURE" == "zynq" ] ; then
    ARMCOUNT=2
else
    ARMCOUNT=0
fi

# Count microblazes
MICROBLAZECOUNT=$( grep InstPath $SDK_MEMORYINFO | grep -i microblaze | wc -l )

# Print architecture details
echo -e "\e[1;32m✔\e[1;34m ➔ \e[1;35m$ARCHITECTURE\e[0m"

echo -e "\e[1;34mCores:"
echo -e "\e[1;34m\tMicroBlazes: \e[0;36m$MICROBLAZECOUNT"
echo -e "\e[1;34m\tARM Cores:   \e[0;36m$ARMCOUNT"

if [ "$ARCHITECTURE" == "zynq" ] ; then
    # Create SDK for ARM core 0
    LD_LIBRARY_PATH=/usr/lib/x86_64-linux-gnu/libstdc++.so.6.0.21:/usr/local/lib $XSDK -batch -source $CREATEARMSDK_SCRIPT -tclargs 0

    # Compile SDK for ARM core 0
    CWD=$(pwd)
    cd $XSDKSDKDIR/board_arm0
    echo -e "\e[1;34mBuilding Board-Driver for ARM CortexA9 0\e[0m"
    make --silent > /dev/null
    if [ $? -ne 0 ] ; then
        echo -e "\e[1;31mmake FAILED!\e[0m"
        exit 1
    else
        echo -e "\e[0;32mmake succeeded.\e[0m"
    fi
    cd $CWD

    # Exporting SDK
    LIBDIR=$SDKDIR/lib/arm0
    INCDIR=$SDKDIR/include/arm0
    mkdir -p $LIBDIR
    mkdir -p $INCDIR

    echo -e "\e[1;34mOrganize SDK directory for ARM CortexA9 0\e[0m"
    echo -e -n "\t\e[1;34m$LIBDIR: "
    cp -r $XSDKSDKDIR/board_arm0/ps7_cortexa9_0/lib/*.a $LIBDIR/.
    if [ $? -ne 0 ] ; then
        echo -e "\e[1;31mFAILED!\e[0m"
        exit 1
    else
        echo -e "\e[1;32m✔\e[0m"
    fi

    echo -e -n "\t\e[1;34m$INCDIR: "
    cp -r $XSDKSDKDIR/board_arm0/ps7_cortexa9_0/include/* $INCDIR/.
    if [ $? -ne 0 ] ; then
        echo -e "\e[1;31mFAILED!\e[0m"
        exit 1
    else
        echo -e "\e[1;32m✔\e[0m"
    fi

    LDSCRIPT="$XSDKSDKDIR/software_arm0/src/lscript.ld"
    echo -e -n "\t\e[1;34m$SDKDIR/ldscript_arm0.ld: "
    cp $LDSCRIPT        $SDKDIR/ldscript_arm0.ld
    if [ $? -ne 0 ] ; then
        echo -e "\e[1;31mFAILED!\e[0m"
        exit 1
    else
        echo -e "\e[1;32m✔\e[0m"
    fi

    # Building PS7_INIT
    echo -e "\e[1;34mBuilding ps7_init library\e[0m"
    CFLAGS="-O0 -c -Wall -fmessage-length=0 -mcpu=cortex-a9 -mfpu=vfpv3 -mfloat-abi=hard"
    GCC=arm-none-eabi-gcc
    AR=arm-none-eabi-ar
    $GCC $CFLAGS -I$SDKDIR -I$SDKDIR/include/arm0 -o $SDKDIR/ps7_init.o     $SDKDIR/ps7_init.c
    $GCC $CFLAGS -I$SDKDIR -I$SDKDIR/include/arm0 -o $SDKDIR/ps7_init_gpl.o $SDKDIR/ps7_init_gpl.c
    $AR r $SDKDIR/lib/arm0/libps7init.a $SDKDIR/ps7_init_gpl.o $SDKDIR/ps7_init.o
    cp $SDKDIR/*.h $SDKDIR/include/arm0/.


fi  # if architecture is zynq



# Generate SDK all MicroBlazes
echo -e "\e[1;34mGenerating XSDK SDK for $MICROBLAZECOUNT MicroBlazes in \e[0;36m$XSDKSDKDIR\e[1;34m using following TCL-Script: \e[0;36m$CREATEMBSDK_SCRIPT\e[0m"
for MBNR in $(seq 0 $((MICROBLAZECOUNT - 1)) ) ; do 
    # Create SDK
    LD_LIBRARY_PATH=/usr/lib/x86_64-linux-gnu/libstdc++.so.6.0.21:/usr/local/lib $XSDK -batch -source $CREATEMBSDK_SCRIPT -tclargs $MBNR
    if [ $? -ne 0 ] ; then
        echo -e "\e[1;31mXSDK FAILED!\e[0m"
        exit 1
    else
        echo -e "\e[0;32mXSDK succeeded.\e[0m"
    fi
    # Check if creation was successful
    if [ ! -d "$XSDKSDKDIR/board_mb$MBNR" ] ; then
        echo -e "\e[1;31mBoard-Drivers in $XSDKSDKDIR/board_mb$MBNR missing!"
        exit 1
    fi
done

# Compile SDK
for MBNR in $(seq 0 $((MICROBLAZECOUNT - 1)) ) ; do 
    CWD=$(pwd)
    cd $XSDKSDKDIR/board_mb$MBNR
    echo -e "\e[1;34mBuilding Board-Driver for MicroBlaze $MBNR\e[0m"
    make --silent > /dev/null
    if [ $? -ne 0 ] ; then
        echo -e "\e[1;31mmake FAILED!\e[0m"
        exit 1
    else
        echo -e "\e[0;32mmake succeeded.\e[0m"
    fi
    cd $CWD
done


#  export SDK
echo -e "\e[1;34mOrganize SDK directory for all MicroBlazes\e[0m"
for MBNR in $(seq 0 $((MICROBLAZECOUNT - 1)) ) ; do 
    LIBDIR=$SDKDIR/lib/mb$MBNR
    INCDIR=$SDKDIR/include/mb$MBNR
    mkdir -p $LIBDIR
    mkdir -p $INCDIR

    echo -e -n "\t\e[1;34m$LIBDIR: "
    cp -r $XSDKSDKDIR/board_mb$MBNR/microblaze_$MBNR/lib/libxil.a $LIBDIR/.
    if [ $? -ne 0 ] ; then
        echo -e "\e[1;31mFAILED!\e[0m"
        exit 1
    else
        echo -e "\e[1;32m✔\e[0m"
    fi

    echo -e -n "\t\e[1;34m$INCDIR: "
    cp -r $XSDKSDKDIR/board_mb$MBNR/microblaze_$MBNR/include/* $INCDIR/.
    if [ $? -ne 0 ] ; then
        echo -e "\e[1;31mFAILED!\e[0m"
        exit 1
    else
        echo -e "\e[1;32m✔\e[0m"
    fi

    LDSCRIPT="$XSDKSDKDIR/software_mb$MBNR/src/lscript.ld"
    echo -e -n "\t\e[1;34m$SDKDIR/ldscript_mb$MBNR.ld: "
    cp $LDSCRIPT        $SDKDIR/ldscript_mb$MBNR.ld
    if [ $? -ne 0 ] ; then
        echo -e "\e[1;31mFAILED!\e[0m"
        exit 1
    else
        echo -e "\e[1;32m✔\e[0m"
    fi
done

echo -e "\e[1;32mSDK created.\e[0m"


# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

