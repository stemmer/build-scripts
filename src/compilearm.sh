#!/usr/bin/env bash
#
# Version: 1.0.0
#
# Changelog:
#  1.0.0 - 11.03.19: First release
#
# Contributors:
#  Ralf Stemmer - ralf.stemmer@uni-oldenburg.de
#

source ./scripts/settings.sh
LIBPATHS="-L$SDKDIR/lib"
INCLUDEPATHS="-I$SDKDIR/include"
CFLAGS="-O0 -c -Wall -fmessage-length=0 -mcpu=cortex-a9 -mfpu=vfpv3 -mfloat-abi=hard -DFSBL_DEBUG -DFSBL_DEBUG_INFO"
LFLAGS="-mcpu=cortex-a9 -mfpu=vfpv3 -mfloat-abi=hard -Wl,-build-id=none"
GCC=arm-none-eabi-gcc

if [ -d "$SW_BUILDDIR" ] ; then
    echo -e -n "\e[1;34mRemoving old object-files: "
    find "$SW_BUILDDIR" -type f -name "*.o" -delete
    echo -e "\e[1;32m✔\e[0m"
fi

mkdir -p $SW_BUILDDIR

function CompileForProcessor
{
    # Compile
    echo -e "\e[1;36mCompiling for ARM CortexA9 $1 \e[0;36m…\e[0m"

    echo -e -n "\e[1;34m$SDKDIR/include/arm$1: "
    if [ ! -d "$SDKDIR/include/arm$1" ] ; then
        echo -e "\e[1;31m$SDKDIR/include/arm$1 missing! Rebuild SDK or check ARM number!"
        exit 1
    else
        echo -e "\e[1;32m✔\e[0m"
    fi

    mkdir -p $SW_BUILDDIR/arm$1

    SOURCES=$(find $SW_SRCDIR/arm$1 -type f -name "*.[c,S]")
    for s in $SOURCES ;
    do
        echo -e -n "\e[1;34m - $s\e[0m "
        FILENAME="${s##*/}"
        OBJFILE="${FILENAME%.*}.o"
        $GCC $CFLAGS $LIBPATHS $INCLUDEPATHS -I$SDKDIR/include/arm$1 -I$SW_SRCDIR/arm$1 -I$SW_SRCDIR/share -c -o "$SW_BUILDDIR/arm$1/$OBJFILE" $s
        if [ $? -ne 0 ] ; then
            echo -e "\e[1;31m$GCC FAILED!\e[0m"
            exit 1
        else
            echo -e "\e[1;32m✔\e[0m"
        fi
    done
}

# Get ARM cores
ARCHITECTURE=$(echo 'cat //Project/SYSTEMINFO/@ARCH' | xmllint --shell "$SDK_SYSTEMDEF" | grep ARCH | grep -v cat | cut -d "\"" -f 2)
if [ "$ARCHITECTURE" == "zynq" ] ; then
    ARMCOUNT=2
else
    echo -e "\e[1;33mThere are no ARM cores on the $ARCHITECTURE plattform!"
    exit 0
fi


# Compile firmware for each ARM
for ARMNR in $(seq 0 $((ARMCOUNT - 1)) ) ; do
    # Only compile if there is source code
    if [ ! -d "$SW_SRCDIR/arm$ARMNR" ] ; then
        echo -e "\e[1;33mThere is no source code for ARM$ARMNR! \e[1;30m(Skipping)\e[0m"
        continue
    fi
    CompileForProcessor $ARMNR
done


# Linken
function LinkFirmwareForProcessor
{
    SOURCEDIR="$SW_SRCDIR/arm$1"
    PROCSOURCES=$(find $SW_BUILDDIR/arm$1  -type f -name "*.o")
    FIRMWARE="$OUTPUTDIR/firmware_arm$1.elf"
    # prefere explicit linker script. Use SDK generated as fallback
    if [ -f $SOURCEDIR/ldscript.ld ] ; then
        LINKSCRIPT="$SOURCEDIR/ldscript.ld"
    elif [ -f $SOURCEDIR/lscript.ld ] ; then    # SDK uses lscript as name
        LINKSCRIPT="$SOURCEDIR/lscript.ld"
    else
        LINKSCRIPT="$SDKDIR/ldscript_arm$1.ld"
    fi

    echo -e "\e[1;36mLinking \e[0;36m$SW_FIRMWARE\e[1;34m: "

    echo -e -n "\e[1;34m$LINKSCRIPT: "
    if [ ! -f "$LINKSCRIPT" ] ; then
        echo -e "\e[1;31m$LINKSCRIPT missing! Rebuild SDK or check ARM number!"
        exit 1
    else
        echo -e "\e[1;32m✔\e[0m"
    fi

    echo -e -n "\e[1;34m$SDKDIR/lib/arm$1: "
    if [ ! -d "$SDKDIR/lib/arm$1" ] ; then
        echo -e "\e[1;31m$SDKDIR/lib/arm$1 missing! Rebuild SDK or check ARM number!"
        exit 1
    else
        echo -e "\e[1;32m✔\e[0m"
    fi

    echo -e -n "\e[1;34mLinking… "
    $GCC -Wl,-T -Wl,$LINKSCRIPT $LFLAGS $LIBPATHS -L$SDKDIR/lib/arm$1 -o $FIRMWARE $PROCSOURCES -lps7init \
        -Wl,--start-group,-lxil,-lgcc,-lc,--end-group \
        -Wl,--start-group,-lxilffs,-lxil,-lgcc,-lc,--end-group 
    if [ $? -ne 0 ] ; then
        echo -e "\e[1;31mFAILED!\e[0m"
        exit 1
    else
        echo -e "\e[1;32m✔\e[0m"
    fi
    chmod -x $FIRMWARE
}

# Link firmware for each microblaze
for ARMNR in $(seq 0 $((ARMCOUNT - 1)) ) ; do 
    # Only compile if there is source code
    if [ ! -d "$SW_SRCDIR/arm$ARMNR" ] ; then
        echo -e "\e[1;33mThere is no source code for ARM$ARMNR! \e[1;30m(Skipping)\e[0m"
        continue
    fi
    LinkFirmwareForProcessor $ARMNR
done

echo -e "\e[1;32mNew ARM binary built.\e[0m"

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

