#!/usr/bin/env bash
#
# Version: 1.0.0
#
# Changelog:
#  1.0.0 - 11.03.19: First release
#
# Contributors:
#  Ralf Stemmer - ralf.stemmer@uni-oldenburg.de
#

source ./scripts/settings.sh

echo -e "\e[1;34mExecuting upload process \e[0;36m…"
LD_LIBRARY_PATH=/usr/lib/x86_64-linux-gnu/libstdc++.so.6.0.21:/usr/local/lib $XSDK -batch -source $UPLOADSCRIPT
if [ $? -ne 0 ] ; then
    echo -e "\e[1;31mFAILED!\e[0m"
    exit 1
else
    echo -e "\e[1;32mSucceeded\e[0m"
fi

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

