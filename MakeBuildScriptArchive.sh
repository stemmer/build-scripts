#!/usr/bin/env bash

echo -e "\e[1;31;40m These scripts are deprecated! \e[0m"

cd ./src
tar -cf ../BuildScripts.tar *
cd ..
gzip BuildScripts.tar

