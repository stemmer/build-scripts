#!/usr/bin/env bash
#
# Version: 0.2.0 - Alpha
#
# Changelog:
#  0.2.4 - 21.03.19: Default installation path changed to /opt
#  0.2.3 - 21.03.19: Missing dependency download script call added to 2nd stage GCC build
#  0.2.2 - 21.03.19: --with-newlib added to Cortex-A9 2nd stage GCC compilation flags
#  0.2.1 - 21.03.19: Exit if user is root
#  0.2.0 - 07.03.19: Build ARM Cortex-A9 Cross Compiler
#  0.1.0 - 06.03.19: Build MicroBlaze Cross Compiler
#
# Contributors:
#  Ralf Stemmer - ralf.stemmer@uni-oldenburg.de
#

# This directory is used to download the source code and build the Compiler.
# After installation, this directory can be removed.
TMPDIR="$HOME/.bstemp"
MBXCINSTALLPATH="/opt/mbxc"
A9XCINSTALLPATH="/opt/armxc"

#GCC_VERSION="6.3.0"
GCC_VERSION="8.2.0"
#BINUTILS_VERSION="2.27"
BINUTILS_VERSION="2.31.1"
NEWLIB_VERSION="3.1.0"

# TODO: Make user do this:
#su
#mkdir /opt/mbxc
#chown -R ralf:ralf /opt/mbxc
#exit
# TODO: Check permissions - no run for root
if [ "$(id -u)" == 0 ] ; then
    echo -e "\e[1;31mThis script shall  not  be executed with root permissions!\e[0m"
    exit 1
fi

# Create temporary director for building the compiler
mkdir -p $TMPDIR


CURDIR="$pwd"

function ExtractArchive {   # Archive-file Extracted-directory-name
    ARCHIVE=$1
    DIRNAME=$2
    echo -e -n "\e[1;34mExtracting \e[0;36m$ARCHIVE\e[0m "
    if [ ! -f "$ARCHIVE" ]; then
        echo -e "\e[1;31m✘\e[1;30m (File \"$ARCHIVE\" missing)\e[0m"
        exit 1
    fi

    if [ -d "$DIRNAME" ]; then
        echo -e "\e[1;33m✔\e[1;30m (Directory \"$DIRNAME\" already existing)\e[0m"
        return
    fi

    tar xf $ARCHIVE

    if [ ! -d "$DIRNAME" ]; then
        echo -e "\e[1;31m✘\e[1;30m (Directory \"$DIRNAME\" not unpacked)\e[0m"
        exit 1
    fi
    echo -e "\e[1;32m✔\e[0m"
}

function DownloadArchive { # URL(without archive name) ARCHIVE
    URL=$1/$2
    FILENAME=$2

    echo -e -n "\e[1;34mDownloading \e[0;36m$FILENAME\e[0m "
    if [ -f "$FILENAME" ]; then
        echo -e "\e[1;33m✔\e[1;30m (File \"$FILENAME\" already downloaded)\e[0m"
        return
    fi

    wget -o ./wget.log $URL

    if [ ! -f "$FILENAME" ]; then
        echo -e "\e[1;31m✘\e[1;30m (Downloading \"$FILENAME\" failed. See wget.log)\e[0m"
        exit 1
    fi

    echo -e "\e[1;32m✔\e[0m"
    rm ./wget.log   # on success, the log file is not needed anymore
}


function DownloadSourceCode {
    cd $TMPDIR
    DownloadArchive ftp://ftp.fu-berlin.de/unix/languages/gcc/releases/gcc-$GCC_VERSION gcc-$GCC_VERSION.tar.gz
    DownloadArchive ftp://sourceware.org/pub/newlib newlib-$NEWLIB_VERSION.tar.gz
    DownloadArchive http://ftp.gnu.org/gnu/binutils binutils-$BINUTILS_VERSION.tar.gz
    cd $CURDIR
}


function BuildBinutils {    # target:"MicroBlaze","Cortex-A9"
    TARGETID="$1"

    # Check if already installed
    cd $TMPDIR
    if [ -f ./binutils.$TARGETID.flag ]; then
        echo -e "\e[1;33mbinutis already installed\e[0m"
        return
    fi

    ExtractArchive binutils-$BINUTILS_VERSION.tar.gz binutils-$BINUTILS_VERSION
    cd $TMPDIR/binutils-$BINUTILS_VERSION

    # Prepare configuration
    echo -e -n "Preparing configuration "
    if [ "$TARGETID" = "MicroBlaze" ] ; then
        PREFIX=$MBXCINSTALLPATH
        PPREFIX="mb-"
        TARGET="microblazeel-xilinx-elf"
    elif [ "$TARGETID" = "Cortex-A9" ] ; then
        PREFIX=$A9XCINSTALLPATH
        PPREFIX="arm-none-eabi-"
        TARGET="arm-none-eabi"
    else
        echo -e "\e[1;31m✘\e[1;30m (Invalid Target \"$TARGETID\". Valid: \"MicroBlaze\", \"Cortex-A9\")\e[0m"
        exit 1
    fi
    echo -e "\e[1;32m✔\e[0m"

    # Run build process
    echo -e "Configuring source code"
    ./configure --prefix=$PREFIX --program-prefix=$PPREFIX --target=$TARGET
    if [ $? -ne 0 ] ; then
        echo -e "\e[1;31mfailed! \e[1;30m(Install missing dependencies and rerun this script)\e[0m"
    fi

    make
    make install
    make clean

    # Flag as complete
    cd $TMPDIR
    touch ./binutils.$TARGETID.flag
    rm -rf binutils-$BINUTILS_VERSION
    cd $CURDIR
    echo -e "\e[1;32mbinutils successfully installed.\e[0m"
}


function BuildGCCStage1 {
    TARGETID="$1"

    # Check if already installed
    cd $TMPDIR
    if [ -f ./gccstage1.$TARGETID.flag ]; then
        echo -e "\e[1;33mGCC Stage 1 already installed\e[0m"
        return
    fi

    ExtractArchive gcc-$GCC_VERSION.tar.gz gcc-$GCC_VERSION

    # Prepare configuration
    echo -e -n "\e[1;34mPreparing configuration \e[0m"
    if [ "$TARGETID" = "MicroBlaze" ] ; then
        PREFIX=$MBXCINSTALLPATH
        PPREFIX="mb-"
        TARGET="microblazeel-xilinx-elf"
        FLAGS="--enable-languages=c --disable-nls --without-headers --disable-multilib --disable-libssp --with-newlib"
    elif [ "$TARGETID" = "Cortex-A9" ] ; then
        PREFIX=$A9XCINSTALLPATH
        PPREFIX="arm-none-eabi-"
        TARGET="arm-none-eabi"
        FLAGS="--without-headers --enable-boostrap --enable-languages=c --disable-threads --enable-__cxa_atexit --disable-libmudflap --with-gnu-ld --with-gnu-as --disable-libssp --disable-libgomp --disable-nls --disable-shared --with-arch=armv7-a --with-tune=cortex-a9 --with-fpu=neon --with-float=hard"
        # Source: http://ibuzzlog.blogspot.com/2014/12/how-to-build-gcc-arm-cross-compiler-for.html
    else
        echo -e "\e[1;31m✘\e[1;30m (Invalid Target \"$TARGETID\". Valid: \"MicroBlaze\", \"Cortex-A9\")\e[0m"
        exit 1
    fi
    echo -e "\e[1;32m✔\e[0m"
    
    cd $TMPDIR/gcc-$GCC_VERSION
 
    ./contrib/download_prerequisites
 
    mkdir build
    cd build/
 
    ../configure --prefix=$PREFIX --program-prefix=$PPREFIX --target=$TARGET $FLAGS
    make all-host
    make install-host

# FOR ARM?
#make -j4 -k all-target-libgcc
#make -i install-target-libgcc

    make clean
    cd ..
    rm -r build

    # Flag as complete
    cd $TMPDIR
    touch ./gccstage1.$TARGETID.flag
    rm -rf gcc-$GCC_VERSION
    cd $CURDIR
    echo -e "\e[1;32mGCC successfully installed.\e[0m"
}


function BuildNewlib {

    TARGETID="$1"

    # Check if already installed
    cd $TMPDIR
    if [ -f ./newlib.$TARGETID.flag ]; then
        echo -e "\e[1;33mGCC Stage 1 already installed\e[0m"
        return
    fi
    ExtractArchive newlib-$NEWLIB_VERSION.tar.gz newlib-$NEWLIB_VERSION

    # Prepare configuration
    echo -e -n "\e[1;34mPreparing configuration \e[0m"
    if [ "$TARGETID" = "MicroBlaze" ] ; then
        PREFIX=$MBXCINSTALLPATH
        PPREFIX="mb-"
        TARGET="microblazeel-xilinx-elf"
        FLAGS="--enable-newlib-hw-fp"
    elif [ "$TARGETID" = "Cortex-A9" ] ; then
        PREFIX=$A9XCINSTALLPATH
        PPREFIX="arm-none-eabi-"
        TARGET="arm-none-eabi"
        FLAGS="--enable-newlib-hw-fp --disable-newlib-supplied-syscalls"
    else
        echo -e "\e[1;31m✘\e[1;30m (Invalid Target \"$TARGETID\". Valid: \"MicroBlaze\", \"Cortex-A9\")\e[0m"
        exit 1
    fi
    echo -e "\e[1;32m✔\e[0m"
    
    cd $TMPDIR/newlib-$NEWLIB_VERSION

    export     CC_FOR_TARGET="$PREFIX/bin/$PPREFIX"gcc
    export    CXX_FOR_TARGET="$PREFIX/bin/$PPREFIX"g++
    export    GCC_FOR_TARGET="$PREFIX/bin/$PPREFIX"gcc
    export     AR_FOR_TARGET="$PREFIX/bin/$PPREFIX"ar
    export     AS_FOR_TARGET="$PREFIX/bin/$PPREFIX"as
    export     LD_FOR_TARGET="$PREFIX/bin/$PPREFIX"ld
    export     NM_FOR_TARGET="$PREFIX/bin/$PPREFIX"nm
    export RANLIB_FOR_TARGET="$PREFIX/bin/$PPREFIX"ranlib
    export  STRIP_FOR_TARGET="$PREFIX/bin/$PPREFIX"strip
 
 
    mkdir build
    cd build
 
    ../configure --prefix=$PREFIX --target=$TARGET $FLAGS
    make
    make install
    make clean

    cd ..
    rm -r build
 
    unset CC_FOR_TARGET
    unset CXX_FOR_TARGET
    unset GCC_FOR_TARGET
    unset AS_FOR_TARGET
    unset AR_FOR_TARGET
    unset LD_FOR_TARGET
    unset NM_FOR_TARGET
    unset STRIP_FOR_TARGET
    unset RANLIB_FOR_TARGET

    # Flag as complete
    cd $TMPDIR
    touch ./newlib.$TARGETID.flag
    rm -rf newlib-$NEWLIB_VERSION
    cd $CURDIR
    echo -e "\e[1;32mnewlib successfully installed.\e[0m"
}


function BuildGCCStage2 {
    TARGETID="$1"

    # Check if already installed
    cd $TMPDIR
    if [ -f ./gccstage2.$TARGETID.flag ]; then
        echo -e "\e[1;33mGCC Stage 2 already installed\e[0m"
        return
    fi

    ExtractArchive gcc-$GCC_VERSION.tar.gz gcc-$GCC_VERSION

    # Prepare configuration
    echo -e -n "\e[1;34mPreparing configuration \e[0m"
    if [ "$TARGETID" = "MicroBlaze" ] ; then
        PREFIX=$MBXCINSTALLPATH
        PPREFIX="mb-"
        TARGET="microblazeel-xilinx-elf"
        FLAGS="--enable-languages=c,c++ --disable-nls --without-headers --disable-multilib --disable-libssp --with-newlib"
    elif [ "$TARGETID" = "Cortex-A9" ] ; then
        PREFIX=$A9XCINSTALLPATH
        PPREFIX="arm-none-eabi-"
        TARGET="arm-none-eabi"
        FLAGS="--enable-languages=c,c++ --without-headers --disable-multilib --with-float=soft --disable-sjlj-exceptions --disable-nls --disable-libmudflap --disable-libssp --enable-long-longx --enable-libgomp --with-arch=armv7-a --with-tune=cortex-a9 --with-fpu=neon --with-float=hard --with-newlib"
    else
        echo -e "\e[1;31m✘\e[1;30m (Invalid Target \"$TARGETID\". Valid: \"MicroBlaze\", \"Cortex-A9\")\e[0m"
        exit 1
    fi
    echo -e "\e[1;32m✔\e[0m"
    
    cd $TMPDIR/gcc-$GCC_VERSION
 
    ./contrib/download_prerequisites
 
    mkdir build
    cd build/
 
    ../configure --prefix=$PREFIX --program-prefix=$PPREFIX --target=$TARGET $FLAGS
    make all
    make install all
    make clean
    cd ..
    rm -r build

    # Flag as complete
    cd $TMPDIR
    touch ./gccstage2.$TARGETID.flag
    rm -rf gcc-$GCC_VERSION
    cd $CURDIR
    echo -e "\e[1;32mGCC successfully installed.\e[0m"
}


echo -e "\e[1;37mPreparing Source Files\e[0m"
DownloadSourceCode

echo -e "\e[1;37mInstalling binutils\e[0m"
BuildBinutils "MicroBlaze"
echo -e "\e[1;37mInstalling gcc stage 1\e[0m"
BuildGCCStage1 "MicroBlaze"
echo -e "\e[1;37mInstalling newlib\e[0m"
BuildNewlib "MicroBlaze"
echo -e "\e[1;37mInstalling gcc stage 2\e[0m"
BuildGCCStage2 "MicroBlaze"

echo -e "\e[1;37mInstalling binutils\e[0m"
BuildBinutils "Cortex-A9"
echo -e "\e[1;37mInstalling gcc stage 1\e[0m"
BuildGCCStage1 "Cortex-A9"
echo -e "\e[1;37mInstalling newlib\e[0m"
BuildNewlib "Cortex-A9"
echo -e "\e[1;37mInstalling gcc stage 2\e[0m"
BuildGCCStage2 "Cortex-A9"

